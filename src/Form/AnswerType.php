<?php

namespace App\Form;

use App\Entity\Answer;
use App\Entity\ChoiceAnswer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnswerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment')
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $answer = $event->getData();
            $form = $event->getForm();

            if ($answer instanceof ChoiceAnswer) {
                $form->add('value', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, [
                    'label' => $answer->getChoice()->getPrompt(),
                    'choices' => array_combine(
                        $answer->getChoice()->getOptions(),
                        $answer->getChoice()->getOptions()
                    ),
                    'multiple' => $answer->getChoice()->isMultiple(),
                    'expanded' => true,
                ]);
            } else {
                $form->add('value', TextType::class, [
                    'label' => $answer->getCompletion()->getPrompt(),
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Answer::class,
        ]);
    }
}
