<?php

namespace App\Form;

use App\Entity\Patient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cId')
            ->add('name')
            ->add('male')
            ->add('birthDate', DateType::class, [
                'widget'        => 'single_text',
            ])
            ->add('diagnosisDate', DateType::class, [
                'widget'        => 'single_text',
            ])
            ->add('phones', CollectionType::class, [
                'entry_type'    => PhoneType::class,
                'allow_add'     => true,
                'allow_delete'  => true,
                'by_reference'  => false,
            ])
            ->add('district')
            ->add('street')
            ->add('institution')
            ->add('physician')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Patient::class,
        ]);
    }
}
