<?php

namespace App\Controller;

use App\Entity\Patient;
use App\Loader\ExcelLoader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class PatientController
{
    /**
     * @Route("/{page}", requirements={"page"="p_\d+"})
     *
     * @Template()
     */
    public function index(EntityManagerInterface $em, $page = 'p_1')
    {
        $page = preg_replace('/^p_/', '', $page);

        $qb
            = $em
            ->getRepository(Patient::class)
            ->createQueryBuilder('p')
        ;
        $qb
            ->select('p')
            ->orderBy('p.expireAt', 'ASC')
            ->addOrderBy('p.id', 'ASC')
            ->setFirstResult(($page - 1) * 20)
            ->setMaxResults(20);
        ;

        $paginator = new Paginator($qb);

        return [
            'patients'  => $paginator,
            'total'     => ceil(count($paginator) / 20),
            'page'      => $page,
        ];
    }

    /**
     * @Route("/import", methods={"POST"})
     *
     * @Template()
     */
    public function import(Request $request, ExcelLoader $loader)
    {
        $file = $request->files->get('excel');
        $excel = \PHPExcel_IOFactory::load($file->getPathname());

        return [
            'file' => $file,
            'pools' => array_combine(
                array_map(function (\PHPExcel_Worksheet $sheet) {
                    return $sheet->getTitle();
                }, $excel->getAllSheets()),
                array_map(function (\PHPExcel_Worksheet $sheet) use ($loader) {
                    return iterator_to_array($loader->load($sheet));
                }, $excel->getAllSheets())
            ),
        ];
    }

    /**
     * @Route("/{patient}", requirements={"patient"="\d+"})
     *
     * @Template()
     */
    public function show(Patient $patient)
    {
        return [
            'patient' => $patient,
        ];
    }
}
