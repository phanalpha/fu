<?php

namespace App\Controller;

use App\Entity\Patient;
use App\Entity\Phone;
use App\Form\PhoneType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PhoneController
{
    /**
     * @Route("/patients/{patient}/phones", methods="POST")
     */
    public function create(Patient $patient, Request $request, FormFactoryInterface $ff, UrlGeneratorInterface $g, EntityManagerInterface $em)
    {
        $form = $ff->create(PhoneType::class, null, [
            'action' => $g->generate('app_phone_create', [ 'patient' => $patient->getId() ]),
            'method' => 'POST',
        ]);
        $form->handleRequest($request);
        if (!$form->isValid()) {
            throw new BadRequestHttpException();
        }

        $phone = $form->getData();
        $phone->setPatient($patient);
        $em->persist($phone);
        $em->flush();

        return new JsonResponse([
            'url' => $g->generate('app_phone_delete', [ 'patient' => $patient->getId(), 'phone' => $phone->getId() ]),
            'number' => $phone->getNumber(),
        ]);
    }

    /**
     * @Route("/patients/{patient}/phones/{phone}", methods="PUT")
     */
    public function put(Patient $patient, Phone $phone, EntityManagerInterface $em)
    {
        $patient->setPrimary($phone);
        $em->flush();

        return new Response();
    }

    /**
     * @Route("/patients/{patient}/phones/{phone}", methods="DELETE")
     */
    public function delete(Patient $patient, Phone $phone, EntityManagerInterface $em)
    {
        if ($phone->isPrimary()) {
            $patient->setPrimary(null);
        }
        $phone->setRemovedAt(new \DateTime());
        $em->flush();

        return new Response();
    }
}
