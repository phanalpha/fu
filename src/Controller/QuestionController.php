<?php

namespace App\Controller;

use App\Entity\Question;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class QuestionController
{
    /**
     * @Route("/questions")
     *
     * @Template()
     */
    public function index(EntityManagerInterface $em)
    {
        return [ 'questions' => $em->getRepository(Question::class)->findBy([], [ 'id' => 'ASC' ]) ];
    }
}
