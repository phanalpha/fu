<?php

namespace App\Controller;

use App\Entity\Choice;
use App\Form\ChoiceType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ChoiceController
{
    /**
     * @Route("/choices/new")
     *
     * @Template()
     */
    public function new(FormFactoryInterface $ff, UrlGeneratorInterface $g)
    {
        $form = $ff->create(ChoiceType::class, null, [
            'action' => $g->generate('app_choice_create'),
            'method' => 'POST',
        ]);

        return [ 'form' => $form->createView() ];
    }

    /**
     * @Route("/choices")
     *
     * @Template(template="choice/new.html.twig")
     */
    public function create(Request $request, FormFactoryInterface $ff, UrlGeneratorInterface $g, EntityManagerInterface $em)
    {
        $form = $ff->create(ChoiceType::class, null, [
            'action' => $g->generate('app_choice_create'),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);
        if (!$form->isValid()) {
            return [ 'form' => $form->createView() ];
        }

        $choice = $form->getData();
        $em->persist($choice);
        $em->flush();

        return new RedirectResponse($g->generate('app_question_index'));
    }

    /**
     * @Route("/choices/{question}/edit")
     *
     * @Template()
     */
    public function edit(Choice $question, FormFactoryInterface $ff, UrlGeneratorInterface $g)
    {
        $form = $ff->create(ChoiceType::class, $question, [
            'action' => $g->generate('app_choice_update', [ 'question' => $question->getId() ]),
            'method' => 'PUT',
        ]);

        return [ 'form' => $form->createView() ];
    }

    /**
     * @Route("/choices/{question}")
     *
     * @Template(template="choice/edit.html.twig")
     */
    public function update(Choice $question, Request $request, FormFactoryInterface $ff, UrlGeneratorInterface $g, EntityManagerInterface $em)
    {
        $form = $ff->create(ChoiceType::class, $question, [
            'action' => $g->generate('app_choice_update', [ 'question' => $question->getId() ]),
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);
        if (!$form->isValid()) {
            return [ 'form' => $form->createView() ];
        }

        $em->flush();

        return new RedirectResponse($g->generate('app_question_index'));
    }
}
