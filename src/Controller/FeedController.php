<?php

namespace App\Controller;

use App\Entity\Choice;
use App\Entity\ChoiceAnswer;
use App\Entity\CompletionAnswer;
use App\Entity\Feed;
use App\Entity\Patient;
use App\Entity\Question;
use App\Form\FeedType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FeedController
{
    /**
     * @Route("/patients/{patient}/feeds/new", methods="GET")
     *
     * @Template()
     */
    public function new(Patient $patient, FormFactoryInterface $ff, UrlGeneratorInterface $g, EntityManagerInterface $em)
    {
        $form = $ff->create(FeedType::class, $this->forge($em), [
            'action' => $g->generate('app_feed_create', [ 'patient' => $patient->getId() ]),
            'method' => 'POST',
        ]);

        return [
            'form' => $form->createView(),
            'patient' => $patient,
            'questions' => $em->getRepository(Question::class)->findBy([], [ 'id' => 'ASC' ]),
        ];
    }

    /**
     * @Route("/patients/{patient}/feeds", methods="POST")
     *
     * @Template("feed/new.html.twig")
     */
    public function create(Patient $patient, Request $request, FormFactoryInterface $ff, UrlGeneratorInterface $g, EntityManagerInterface $em)
    {
        $form = $ff->create(FeedType::class, $this->forge($em), [
            'action' => $g->generate('app_feed_create', [ 'patient' => $patient->getId() ]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);
        if (!$form->isValid()) {
            return [
                'form' => $form->createView(),
                'patient' => $patient,
                'questions' => $em->getRepository(Question::class)->findBy([], [ 'id' => 'ASC' ]),
            ];
        }

        $feed = $form->getData();
        $feed
            ->setPatient($patient)
            ->setCreatedAt(new \DateTime())
        ;
        foreach ($feed->getAnswers() as $answer) {
            if ($form->get('defer')->isClicked()
                || empty($answer->getValue())) {
                $feed->removeAnswer($answer);
            }
        }
        $patient->setExpireAt(new \DateTime($feed->getAnswers()->count() > 0 ? '+1 year' : '+1 week'));
        $em->persist($feed);
        $em->flush();

        return new RedirectResponse($g->generate('app_patient_show', [ 'patient' => $patient->getId() ]));
    }

    private function forge(EntityManagerInterface $em)
    {
        $feed = new Feed();
        foreach ($em->getRepository(Question::class)->findBy([], [ 'id' => 'ASC' ]) as $q) {
            $feed->addAnswer(
                $q instanceof Choice
                ? (new ChoiceAnswer())->setChoice($q)
                : (new CompletionAnswer())->setCompletion($q)
            );
        }

        return $feed;
    }
}
