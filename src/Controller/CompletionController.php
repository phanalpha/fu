<?php

namespace App\Controller;

use App\Entity\Completion;
use App\Form\CompletionType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CompletionController
{
    /**
     * @Route("/completions/new")
     *
     * @Template()
     */
    public function new(FormFactoryInterface $ff, UrlGeneratorInterface $g)
    {
        $form = $ff->create(CompletionType::class, null, [
            'action' => $g->generate('app_completion_create'),
            'method' => 'POST',
        ]);

        return [ 'form' => $form->createView() ];
    }

    /**
     * @Route("/completions")
     *
     * @Template(template="completion/new.html.twig")
     */
    public function create(Request $request, FormFactoryInterface $ff, UrlGeneratorInterface $g, EntityManagerInterface $em)
    {
        $form = $ff->create(CompletionType::class, null, [
            'action' => $g->generate('app_completion_create'),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);
        if (!$form->isValid()) {
            return [ 'form' => $form->createView() ];
        }

        $completion = $form->getData();
        $em->persist($completion);
        $em->flush();

        return new RedirectResponse($g->generate('app_question_index'));
    }

    /**
     * @Route("/completions/{question}/edit")
     *
     * @Template()
     */
    public function edit(Completion $question, FormFactoryInterface $ff, UrlGeneratorInterface $g)
    {
        $form = $ff->create(CompletionType::class, $question, [
            'action' => $g->generate('app_completion_update', [ 'question' => $question->getId() ]),
            'method' => 'PUT',
        ]);

        return [ 'form' => $form->createView() ];
    }

    /**
     * @Route("/completions/{question}")
     *
     * @Template(template="completion/edit.html.twig")
     */
    public function update(Completion $question, Request $request, FormFactoryInterface $ff, UrlGeneratorInterface $g, EntityManagerInterface $em)
    {
        $form = $ff->create(CompletionType::class, $question, [
            'action' => $g->generate('app_completion_update', [ 'question' => $question->getId() ]),
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);
        if (!$form->isValid()) {
            return [ 'form' => $form->createView() ];
        }

        $em->flush();

        return new RedirectResponse($g->generate('app_question_index'));
    }
}
