<?php

namespace App\Loader;

use App\Form\PatientType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class ExcelLoader
{
    private const COLUMNS = [
        '序号'      => null,
        '病历号'    => 'cId',
        '姓名'      => 'name',
        '性别'      => [
            'field'     => 'male',
            'positive'  => '男',
        ],
        '出生日期'  => [
            'field'     => 'birthDate',
            'type'      => 'date',
        ],
        '诊断日期'  => [
            'field'     => 'diagnosisDate',
            'type'      => 'date',
        ],
        '联系方式'  => [
            'field'     => 'phones.number',
            'ignore'    => [ '-', 'NULL' ],
        ],
        '地区'      => 'district',
        '街道'      => 'street',
        '单位名称'  => 'institution',
        '主诊医师'  => 'physician',
    ];

    private $em;

    private $ff;


    /**
     * Construct
     *
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface   $ff
     */
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $ff)
    {
        $this->em = $em;
        $this->ff = $ff;
    }

    /**
     * Load patients from sheet
     *
     * @param \PHPExcel_Worksheet $sheet
     *
     * @return LaneIterator
     */
    public function load(\PHPExcel_Worksheet $sheet)
    {
        $fields = [];

        foreach ($sheet->getRowIterator() as $row) {
            if ($row->getRowIndex() === 1) {
                foreach ($row->getCellIterator() as $cell) {
                    $title = trim($cell->getValue());
                    if (empty(self::COLUMNS[$title])) {
                        continue;
                    }

                    $fields[$cell->getColumn()] = self::COLUMNS[$title];
                }
            } else {
                $data = [];
                foreach ($row->getCellIterator() as $cell) {
                    if (empty($fields[$cell->getColumn()])) {
                        continue;
                    }

                    $f = $fields[$cell->getColumn()];
                    $v = trim($cell->getValue());
                    if (is_array($f)) {
                        if (array_key_exists('ignore', $f) && in_array($v, $f['ignore'])) {
                            $v = null;
                        }

                        if (array_key_exists('positive', $f)) {
                            $v = $v === $f['positive'];
                        }

                        switch (@$f['type']) {
                        case 'date':
                            $v = (new \DateTime($v))->format('Y-m-d');
                            break;
                        }

                        $fs = explode('.', $f['field']);
                        $f = array_shift($fs);
                        if ($f2 = array_shift($fs)) {
                            $v = $v ? [[$f2 => $v]] : [];
                        }
                    }

                    $data[$f] = array_key_exists($f, $data) ? array_merge($data[$f], $v) : $v;
                }

                $form = $this->ff->create(PatientType::class);
                $form->submit($data);

                $lane = new Lane($row, $form);
                if (!$form->isValid()) {
                    yield $lane;

                    continue;
                }

                $patient = $form->getData();
                $patient->setExpireAt(new \DateTime());
                $this->em->persist($patient);
                $this->em->flush();

                yield $lane->setPatient($patient);
            }
        }
    }
}
