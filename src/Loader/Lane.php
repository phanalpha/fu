<?php

namespace App\Loader;

use App\Entity\Patient;
use Symfony\Component\Form\FormInterface;

class Lane
{
    private $row;

    private $form;

    private $patient;


    public function __construct(\PHPExcel_Worksheet_Row $row, FormInterface $form, Patient $patient = null)
    {
        $this->row = $row;
        $this->form = $form;
        $this->patient = $patient;
    }

    /**
     * @return \PHPExcel_Worksheet_Row
     */
    public function getRow(): \PHPExcel_Worksheet_Row
    {
        return $this->row;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->form;
    }

    /**
     * @return Patient
     */
    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    /**
     * @param Patient $patient
     *
     * @return $this
     */
    public function setPatient(Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }
}
