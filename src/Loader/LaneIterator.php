<?php

namespace App\Loader;

interface LaneIterator extends \Iterator
{
    /**
     * @return Lane
     */
    public function current(): Lane;
}
