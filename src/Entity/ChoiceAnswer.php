<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChoiceAnswerRepository")
 */
class ChoiceAnswer extends Answer
{
    /**
     * @var Choice
     *
     * @ORM\ManyToOne(targetEntity="Choice")
     * @ORM\JoinColumn(nullable=false)
     */
    private $choice;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $value;


    /**
     * @return Choice
     */
    public function getChoice(): ?Choice
    {
        return $this->choice;
    }

    /**
     * @param Choice $choice
     *
     * @return $this
     */
    public function setChoice(Choice $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }
}
