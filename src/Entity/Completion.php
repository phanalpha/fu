<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompletionRepository")
 */
class Completion extends Question
{
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $date;


    public function __construct()
    {
        $this->date = false;
    }

    /**
     * @return bool
     */
    public function isDate(): bool
    {
        return $this->date;
    }

    /**
     * @param bool $date
     *
     * @return $this
     */
    public function setDate(bool $date): self
    {
        $this->date = $date;

        return $this;
    }
}
