<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChoiceRepository")
 */
class Choice extends Question
{
    /**
     * @var string[]
     *
     * @ORM\Column(type="json_array")
     *
     * @Assert\Count(min=2)
     * @Assert\All({
     *     @Assert\NotBlank()
     * })
     */
    private $options;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $multiple;


    public function __construct()
    {
        $this->options = [];
        $this->multiple = false;
    }

    /**
     * @return string[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param string[] $options
     *
     * @return $this
     */
    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    /**
     * @param bool $multiple
     *
     * @return $this
     */
    public function setMultiple(bool $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }
}
