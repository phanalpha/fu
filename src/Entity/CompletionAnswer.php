<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompletionAnswerRepository")
 */
class CompletionAnswer extends Answer
{
    /**
     * @var Completion
     *
     * @ORM\ManyToOne(targetEntity="Completion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $completion;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $value;


    /**
     * @return Completion
     */
    public function getCompletion(): ?Completion
    {
        return $this->completion;
    }

    /**
     * @param Completion $completion
     *
     * @return $this
     */
    public function setCompletion(Completion $completion): self
    {
        $this->completion = $completion;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setValue(string $value = null): self
    {
        $this->value = $value;

        return $this;
    }
}
