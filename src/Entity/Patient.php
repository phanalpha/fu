<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 * @UniqueEntity({"cId"})
 */
class Patient
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=7, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=7)
     */
    private $cId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=20)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $male;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     *
     * @Assert\NotBlank()
     */
    private $diagnosisDate;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Phone", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"id" = "ASC"})
     *
     * @Assert\Valid()
     */
    private $phones;

    /**
     * @var Phone
     *
     * @ORM\OneToOne(targetEntity="Phone")
     */
    private $primary;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=20)
     */
    private $district;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\Length(max=20)
     */
    private $institution;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=20)
     */
    private $physician;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $expireAt;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Feed", mappedBy="patient")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $feeds;


    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->feeds = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCId(): ?string
    {
        return $this->cId;
    }

    /**
     * @param string $cId
     *
     * @return $this
     */
    public function setCId(string $cId): self
    {
        $this->cId = $cId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMale(): ?bool
    {
        return $this->male;
    }

    /**
     * @param bool $male
     *
     * @return $this
     */
    public function setMale(bool $male): self
    {
        $this->male = $male;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     *
     * @return $this
     */
    public function setBirthDate(\DateTime $birthDate = null): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDiagnosisDate(): ?\DateTime
    {
        return $this->diagnosisDate;
    }

    /**
     * @param \DateTime $diagnosisDate
     *
     * @return $this
     */
    public function setDiagnosisDate(\DateTime $diagnosisDate = null): self
    {
        $this->diagnosisDate = $diagnosisDate;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @param Phone $phone
     *
     * @return $this
     */
    public function addPhone(Phone $phone): self
    {
        $this->phones[] = $phone->setPatient($this);

        return $this;
    }

    /**
     * @param Phone $phone
     */
    public function removePhone(Phone $phone)
    {
        $this->phones->removeElement($phone);
    }

    /**
     * @return Phone
     */
    public function getPrimary(): ?Phone
    {
        return $this->primary;
    }

    /**
     * @param Phone $primary
     *
     * @return $this
     */
    public function setPrimary(Phone $primary = null): self
    {
        $this->primary = $primary;
        return $this;
    }

    /**
     * @return string
     */
    public function getDistrict(): ?string
    {
        return $this->district;
    }

    /**
     * @param string $district
     *
     * @return $this
     */
    public function setDistrict(string $district): self
    {
        $this->district = $district;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return $this
     */
    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getInstitution(): ?string
    {
        return $this->institution;
    }

    /**
     * @param string $institution
     *
     * @return $this
     */
    public function setInstitution(string $institution): self
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhysician(): ?string
    {
        return $this->physician;
    }

    /**
     * @param string $physician
     *
     * @return $this
     */
    public function setPhysician(string $physician): self
    {
        $this->physician = $physician;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireAt(): ?\DateTime
    {
        return $this->expireAt;
    }

    /**
     * @param \DateTime $expireAt
     *
     * @return Patient
     */
    public function setExpireAt(\DateTime $expireAt): Patient
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getFeeds(): Collection
    {
        return $this->feeds;
    }

    /**
     * @param Feed $feed
     *
     * @return $this
     */
    public function addFeed(Feed $feed): self
    {
        $this->feeds[] = $feed->setPatient($this);

        return $this;
    }

    /**
     * @param Feed $feed
     */
    public function removeFeed(Feed $feed)
    {
        $this->feeds->removeElement($feed);
    }
}
