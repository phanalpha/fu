<?php

namespace App\Command;

use App\Loader\ExcelLoader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppLoadPatientsCommand extends Command
{
    protected static $defaultName = 'app:load-patients';

    private $loader;


    public function __construct(ExcelLoader $loader)
    {
        parent::__construct();

        $this->loader = $loader;
    }

    protected function configure()
    {
        $this
            ->setDescription('Load patients from Excel')
            ->addArgument('excel', InputArgument::REQUIRED, 'Excel, the document')
            ->addArgument('sheet', InputArgument::OPTIONAL, 'Sheet, the title')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $excel = \PHPExcel_IOFactory::load($input->getArgument('excel'));
        foreach ($input->getArgument('sheet')
                     ? [$excel->getSheetByName($input->getArgument('sheet'))]
                     : $excel->getAllSheets() as $sheet) {
            foreach ($this->loader->load($sheet) as $lane) {
                if ($lane->getPatient()) {
                    $io->success(
                        sprintf(
                            "%d: %s <%d>",
                            $lane->getRow()->getRowIndex(),
                            $lane->getPatient()->getName(),
                            $lane->getPatient()->getId()
                        )
                    );
                } else {
                    foreach ($lane->getForm()->getErrors(true, true) as $error) {
                        $io->warning(
                            sprintf(
                                "%d: %s - %s",
                                $lane->getRow()->getRowIndex(),
                                $error->getOrigin()->getName(),
                                $error->getMessage()
                            )
                        );
                    }
                }
            }
        }
    }
}
